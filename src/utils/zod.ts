import * as z from 'zod'

const errorMap: z.ZodErrorMap = (error, ctx) => {
  // 這裡可寫全域的防呆錯誤訊息
  let customMsg: string | null = null

  switch (error.code) {
    // 輸入太小
    case z.ZodIssueCode.too_small:
      // 無任何輸入
      if (!ctx.data.length) {
        customMsg = '必填欄位'
        break
      }
      if (error.type === 'string')
        customMsg = `輸入長度不可少於 ${error.minimum} 碼!`
      if (error.type === 'number')
        customMsg = `輸入數值不可低於 ${error.minimum} !`
      if (error.type === 'array')
        customMsg = `所選數量不可少於 ${error.minimum} 項!`
      break

    // 輸入過大
    case z.ZodIssueCode.too_big:
      if (error.type === 'string')
        customMsg = `文字長度不可多於 ${error.maximum} 碼!`
      if (error.type === 'number')
        customMsg = `輸入數值不可大於 ${error.maximum} !`
      if (error.type === 'array')
        customMsg = `所選數量不可多於 ${error.maximum} 項!`
      break

    // 內建字串驗證格式錯誤
    case z.ZodIssueCode.invalid_string:
      if (error.validation === 'email')
        customMsg = '電子郵件格式錯誤'
      break

    // 格式錯誤
    case z.ZodIssueCode.invalid_type:
      customMsg = '格式錯誤'
      break
  }
  // 如果無自定義訊息，fallback 到原始錯誤訊息
  return { message: customMsg || ctx.defaultError }
}

z.setErrorMap(errorMap)

export default z
