import type { Dispatch, ReactNode } from 'react'
import { createContext, useContext, useReducer } from 'react'

// 類型宣告
export interface LoadingState {
  isLoading: boolean
  requestSent: number
  dispatch: Dispatch<LoadingAction>
}

interface LoadingAction {
  type: 'triggerLoading'
  payload: boolean | number
}

// 預設值
const defaultValues: LoadingState = {
  isLoading: false,
  requestSent: 0,
  dispatch: () => {},
}

// 宣告 reducer 動作
function reducer(state: LoadingState, action: LoadingAction) {
  const { type, payload } = action
  const clone = { ...state }

  switch (type) {
    case 'triggerLoading':
      // 參數為零，直接將當前 API 計數歸 0 (reset 用)
      if (payload === 0)
        clone.requestSent = 0
      // true API 計數 +1
      else if (payload)
        clone.requestSent++
      // false API 計數-1
      else
        clone.requestSent--
      // 最後看 API 計數來決定是否顯示遮罩
      if (clone.requestSent > 0) {
        clone.isLoading = true
      }
      else {
        clone.requestSent = 0
        clone.isLoading = false
      }
      break
    default:
      break
  }
  return clone
}

// 建立 context
const LoadingContext = createContext<LoadingState>(defaultValues)

// 外拋 context hook
export function useLoadingContext() {
  return useContext(LoadingContext)
}

// 外拋 context provider
export function LoadingProvider({ children }: { children: ReactNode }) {
  const [state, dispatch] = useReducer(reducer, defaultValues)
  const value = {
    isLoading: state.isLoading,
    requestSent: state.requestSent,
    dispatch,
  }

  return (
    <LoadingContext.Provider value={value}>
      {children}
    </LoadingContext.Provider>
  )
}
