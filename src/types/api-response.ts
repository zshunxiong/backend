export interface ApiResponse<T> {
  retCode: 0 | 1 // 0: 成功 、 1: 失敗
  retMsg?: string
  retData?: T
}

export interface SubscriptionType {
  id: string
  userid: string
  skip: boolean
  endpoint: string
}

// GET => subscription
export interface SubscriptionGetResponse extends Array<SubscriptionType> {}
