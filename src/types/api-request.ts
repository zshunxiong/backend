// PUT => subscription/${id}
export interface SubscriptionIdPutRequest {
  skip: boolean
}

// POST => subscription/push
export interface SubscriptionPushPostRequest {
  ids: string[]
  message: string
}
