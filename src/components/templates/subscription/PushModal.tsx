import type { FunctionComponent } from 'react'

import { Button } from '@/components/atoms'
import { Input, Modal } from '@/components/molecules'
import type { ModalProps } from '@/components/molecules/Modal'
import { Form } from '@/components/organisms'
import z from '@/utils/zod'

const schema = z.object({
  message: z.string().min(1),
})

type SchemaType = z.infer<typeof schema>

export interface PushModalProps extends ModalProps {
  onSubmit: (formData: SchemaType) => void
}

const PushModal: FunctionComponent<PushModalProps> = ({
  onSubmit,
  ...props
}) => {
  return (
    <Modal {...props} className='max-w-md'>
      <Form
        className='space-y-3'
        schema={schema}
        onSubmit={onSubmit}
      >
        <Input name='message' placeholder='訊息' />
        <Button ariaLabel='Submit' className='ml-auto flex' type="submit">送出</Button>
      </Form>
    </Modal>
  )
}
PushModal.displayName = 'PushModal'

export default PushModal
