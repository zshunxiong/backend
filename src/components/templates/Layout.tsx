import { faBell, faQuestionCircle, faUser } from '@fortawesome/free-regular-svg-icons'
import { faSpinner } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { AnimatePresence, m } from 'framer-motion'
import Head from 'next/head'
import { useRouter } from 'next/router'
import type { FunctionComponent, ReactNode } from 'react'
import { useMemo } from 'react'

import { LoadingBar } from '@/components/atoms'
import { Overlay } from '@/components/molecules'
import { Dialog, Header, InitPwa } from '@/components/organisms'
import { useDialogContext } from '@/contexts/DialogContext'
import { useLoadingContext } from '@/contexts/LoadingContext'

interface LayoutProps {
  children: ReactNode
}

const Layout: FunctionComponent<LayoutProps> = ({ children }) => {
  const { props } = useDialogContext()
  const { isLoading } = useLoadingContext()
  const router = useRouter()

  const routes = useMemo(() =>
    [
      {
        path: '/',
        title: '推播管理',
        icon: faBell,
      },
      {
        path: '/user',
        title: '使用者管理',
        icon: faUser,
      },
      {
        path: '/404',
        title: '404 Testing',
        icon: faQuestionCircle,
      },
    ]
  , [])

  const currentRoute = useMemo(() =>
    routes.find(({ path }) => path === router.asPath)
  , [routes, router.asPath])

  return (
    <main className="flex h-full w-full flex-col-reverse overflow-hidden md:flex-col">
      {/* html head tag */}
      <Head>
        <title>Backend</title>
        <meta
          name='viewport'
          content='minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no, user-scalable=no, viewport-fit=cover'
        />
      </Head>
      {/* 上方讀取進度條 */}
      <LoadingBar />
      {/* 標頭 */}
      <Header routes={routes} />
      {/* 主要內容區域 (換頁時做動畫) */}
      <AnimatePresence mode='wait'>
        <m.div
          key={router.asPath}
          initial={{ opacity: 0, y: -100 }}
          animate={{ opacity: 1, y: 0 }}
          exit={{ opacity: 0, y: -100 }}
          className='h-[calc(100%-theme(spacing.14))] overflow-y-auto px-[5%] py-4'
        >
          <div className='flex h-full flex-col'>
            {/* TODO: 可做 breadcrumb */}
            <b className='mb-2 text-2xl'>
              {currentRoute?.title}
            </b>
            {children}
          </div>
        </m.div>
      </AnimatePresence>
      {/* 全域的提示彈窗容器 */}
      <Dialog {...props} />
      {/* Pwa 初始化 */}
      <InitPwa />
      {/* 遮罩 & 背景 */}
      <Overlay show={isLoading}>
        <FontAwesomeIcon className='text-6xl text-text-inverse motion-safe:animate-spin' icon={faSpinner} />
      </Overlay>
      <div className="gradient fixed inset-x-0 bottom-[-20vh] -z-10 h-[40vh] blur-[20vh]" />
      <div className="fixed bottom-0 left-0 h-[env(safe-area-inset-bottom)] w-full bg-container-normal" />
    </main>
  )
}
Layout.displayName = 'Layout'

export default Layout
