import type { Table as ReactTable, TableOptions } from '@tanstack/react-table'
import { createColumnHelper, flexRender, getCoreRowModel, useReactTable } from '@tanstack/react-table'
import clsx from 'clsx'
import type { Dispatch, FunctionComponent, HTMLProps, MutableRefObject, SetStateAction } from 'react'
import { useEffect, useMemo, useRef, useState } from 'react'

export interface TableProps<TData> extends Omit<TableOptions<TData>, 'getCoreRowModel' | 'data'> {
  data?: TData
  rowState?: [{}, Dispatch<SetStateAction<{}>>]
  tableRef?: MutableRefObject<ReactTable<TData> | null>
  className?: string
}

/**
 * 封裝 react-table
 *
 * @param columns react-table 欄位定義
 * @param data table 資料
 * @param enableRowSelection 允許勾選的 row，這個有給就會渲染勾選框
 * @param tableRef react useRef 用來存放 useReactTable 的回傳值，方便操作 table
 * @param className 額外 class
 */
const Table: FunctionComponent<TableProps<any>> = (
  {
    columns,
    data = [],
    enableRowSelection,
    rowState,
    tableRef,
    className,
  },
) => {
  const localRowState = useState({})

  const columnHelper = createColumnHelper<any>()
  // 預處理 columns
  const preprocessColumns = useMemo(
    () => [
      ...(
        // 若有給 enableRowSelection，直接在前面加勾選框
        enableRowSelection
          ? [columnHelper.accessor('select', {
              header: ({ table }) => (
                <div className="flex items-center justify-center px-1">
                  <IndeterminateCheckbox
                    {...{
                      checked: table.getIsAllRowsSelected(),
                      indeterminate: table.getIsSomeRowsSelected(),
                      onChange: table.getToggleAllRowsSelectedHandler(),
                    }}
                  />
                </div>
              ),
              cell: ({ row }) => (
                <div className="flex items-center justify-center px-1">
                  <IndeterminateCheckbox
                    {...{
                      checked: row.getIsSelected(),
                      disabled: !row.getCanSelect(),
                      indeterminate: row.getIsSomeSelected(),
                      onChange: row.getToggleSelectedHandler(),
                    }}
                  />
                </div>
              ),
            })]
          : []),
      ...columns,
    ],
    [enableRowSelection, columnHelper, columns],
  )
  // 看有沒有提供 rowState，有的話用 parent 的，否則用 local 的
  const rowSelection = useMemo(() => rowState ? rowState[0] : localRowState[0], [rowState, localRowState])
  const onRowSelectionChange = useMemo(() => rowState ? rowState[1] : localRowState[1], [rowState, localRowState])

  const table = useReactTable({
    columns: preprocessColumns,
    data,
    state: {
      rowSelection,
    },
    enableRowSelection,
    onRowSelectionChange,
    getCoreRowModel: getCoreRowModel(),
  })

  useEffect(() => {
    if (tableRef)
      tableRef.current = table
  }, [tableRef, table])

  return (
    <div className={clsx(['w-full overflow-auto', className])}>
      <table className='w-full min-w-max border bg-container-normal/20 text-center'>
        <thead>
          {table.getHeaderGroups().map(headerGroup => (
            <tr key={headerGroup.id}>
              {headerGroup.headers.map(header => (
                <th className='border bg-primary/10 px-2 py-1' key={header.id}>
                  {header.isPlaceholder
                    ? null
                    : flexRender(
                      header.column.columnDef.header,
                      header.getContext(),
                    )}
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody>
          {table.getRowModel().rows.map(row => (
            <tr key={row.id}>
              {row.getVisibleCells().map(cell => (
                <td className='border px-2 py-1' key={cell.id}>
                  {flexRender(cell.column.columnDef.cell, cell.getContext())}
                </td>
              ))}
            </tr>
          ))}
          {!table.getRowModel().rows.length && (
            <tr>
              <td colSpan={preprocessColumns.length} className='border px-2 py-1'>暫無資料</td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  )
}
Table.displayName = 'Table'

// 勾選框
function IndeterminateCheckbox({
  indeterminate,
  className = '',
  ...rest
}: { indeterminate?: boolean } & HTMLProps<HTMLInputElement>) {
  const ref = useRef<HTMLInputElement>(null!)

  useEffect(() => {
    if (typeof indeterminate === 'boolean')
      ref.current.indeterminate = !rest.checked && indeterminate
  }, [ref, indeterminate, rest.checked])

  return (
    <input
      type="checkbox"
      aria-label={`Checkbox for ${rest.name}`}
      ref={ref}
      className={clsx({
        'text-primary focus:ring-offset-container-normal focus:ring-primary rounded-sm': true,
        'cursor-pointer': !rest.disabled,
        'bg-gray-200 opacity-40': rest.disabled,
        className,
      })}
      {...rest}
    />
  )
}
IndeterminateCheckbox.displayName = 'IndeterminateCheckbox'

export default Table
