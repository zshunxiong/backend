import { faCircleCheck, faCircleXmark, faQuestionCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import clsx from 'clsx'
import type { FunctionComponent, KeyboardEvent, MouseEvent } from 'react'
import { useCallback } from 'react'

import { Button } from '@/components/atoms'
import { Modal } from '@/components/molecules'

import type { ModalProps } from '../molecules/Modal'

export interface DialogProps extends Pick<ModalProps, 'show' | 'close' | 'onClose' | 'title' | 'children'> {
  variant?: 'primary' | 'success' | 'danger'
  message?: string
  hideCancel?: boolean
  onConfirm?: (close: NonNullable<ModalProps['close']>) => void
}

/**
 * 自定義提示彈窗 (引用 molecules/Modal)
 *
 * @param show 是否顯示
 * @param close 關閉彈窗的方法
 * @param variant 提示彈窗的類型 ('primary' | 'success' | 'danger')
 * @param title 提示彈窗標題
 * @param message 提示彈窗內容
 * @param hideCancel 是否隱藏取消按鈕 (若沒給 onConfirm 本來就不會出現)
 * @param onConfirm 點擊確定後的動作 (若沒給點擊後會關閉彈窗)
 * @param children ReactNode
 */
const Dialog: FunctionComponent<DialogProps> = ({
  show = false,
  close = () => {},
  variant = 'primary',
  title = '提示',
  message,
  hideCancel,
  onConfirm,
  children,
}) => {
  const colors = {
    primary: {
      text: 'text-primary',
      line: 'border-primary',
      bg: 'bg-primary',
      button: 'primary',
      icon: faQuestionCircle,
    },
    success: {
      text: 'text-green-500',
      line: 'border-green-500',
      bg: 'bg-green-500',
      button: 'green',
      icon: faCircleCheck,
    },
    danger: {
      text: 'text-red-500',
      line: 'border-red-500',
      bg: 'bg-red-500',
      button: 'red',
      icon: faCircleXmark,
    },
  } as const

  const handleConfirm = useCallback((e: MouseEvent | KeyboardEvent) => {
    if (onConfirm)
      onConfirm(close)
    else close(e)
  }, [close, onConfirm])

  return (
    <Modal className="max-w-lg" show={show} close={close}>
      <div className="z-10 flex w-full flex-col items-center justify-center py-2 text-center">
        {/* Icon */}
        <FontAwesomeIcon
          className={clsx(['mb-4 text-7xl', colors[variant].text])}
          icon={colors[variant].icon}
        />
        {/* 內容 */}
        <b className={clsx(['text-xl', colors[variant].text])}> {title} </b>
        <span className="text-text-normal"> {message} </span>
        {children}
        {/* 按鈕動作 */}
        <div className="z-10 mt-10 flex w-5/6 items-center justify-center space-x-2">
          {
            onConfirm && !hideCancel
              && <Button
                ariaLabel='Cancel'
                color="transparent"
                className={colors[variant].text}
                onClick={close}
              > 取消 </Button>
          }
          <div className="rounded-md bg-container-normal">
            <Button ariaLabel='Confirm' color={colors[variant].button} onClick={handleConfirm}> 確認 </Button>
          </div>
        </div>
      </div>
      {/* 三角形區塊 */}
      <div className={clsx(['absolute -bottom-24 -left-10 h-48 w-48 rotate-45 rounded-md opacity-10 md:opacity-20', colors[variant].bg])}></div>
      <div className={clsx(['absolute -bottom-24 left-14 h-40 w-40 rotate-45 rounded-md opacity-5', colors[variant].bg])}></div>
      <div className={clsx(['absolute -bottom-28 left-32 h-48 w-48 rotate-45 rounded-md opacity-5', colors[variant].bg])}></div>
      <div className={clsx(['absolute -bottom-44 left-64 hidden h-44 w-44 rotate-45 rounded-md opacity-10 md:block', colors[variant].bg])}></div>
      <div className={clsx(['absolute -bottom-24 right-60 h-40 w-40 rotate-45 rounded-md opacity-5', colors[variant].bg])}></div>
      <div className={clsx(['absolute -bottom-44 right-32 hidden h-48 w-48 rotate-45 rounded-md opacity-10 md:block', colors[variant].bg])}></div>
      <div className={clsx(['absolute -bottom-32 right-10 hidden h-48 w-48 rotate-45 rounded-md opacity-10 md:block', colors[variant].bg])}></div>
      <div className={clsx(['absolute -bottom-40 -right-24 h-60 w-60 rotate-45 rounded-md opacity-20', colors[variant].bg])}></div>
    </Modal>
  )
}
Dialog.displayName = 'Dialog'

export default Dialog
