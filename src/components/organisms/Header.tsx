import { faUserCircle } from '@fortawesome/free-regular-svg-icons'
import type { IconDefinition } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import clsx from 'clsx'
import { useRouter } from 'next/router'
import type { FunctionComponent } from 'react'
import { useEffect } from 'react'
import { useDarkMode, useMediaQuery } from 'usehooks-ts'

import { Button, DarkmodeSwitch } from '@/components/atoms'
import { Dropdown } from '@/components/molecules'

interface HeaderProps {
  routes: Array<{
    path: string
    title: string
    icon: IconDefinition
  }>
  className?: string
}

/**
 * Header
 *
 * @param routes 路徑設定
 * @param className 額外 class
 */
const Header: FunctionComponent<HeaderProps> = ({
  routes,
  className,
}) => {
  const { push, asPath } = useRouter()
  const { isDarkMode, toggle } = useDarkMode()
  const md = useMediaQuery('(min-width: 768px)')

  useEffect(() => {
    const html = document.querySelector('html')
    if (html) {
      if (isDarkMode)
        html.classList.add('dark')
      else
        html.classList.remove('dark')
    }
  }, [isDarkMode])

  return (
    <div className={clsx([
      'z-[1] flex h-14 w-full items-center justify-around bg-container-normal',
      'shadow-[0_-25px_25px_-20px] !shadow-container-inverse/20',
      'md:justify-end md:pl-6 md:shadow-lg',
      className,
    ])}
    >
      <div className='mr-auto hidden md:block'>Logo</div>
      {
        routes.map(({ path, icon }) =>
          <Button
            key={path}
            ariaLabel='Redirection'
            variant='header'
            className={clsx({
              'text-primary border-b-primary': path === asPath,
            })}
            onClick={() => push(path)}
          >
            <FontAwesomeIcon icon={icon}/>
          </Button>,
        )
      }
      <Dropdown direction={md ? 'bottom' : 'top'} as={
        <Button ariaLabel='Dropdown' variant='header'>
          <FontAwesomeIcon icon={faUserCircle}/>
        </Button>
      }>
        <div className='flex w-40 flex-col items-end space-y-2 border bg-container-normal p-2'>
          <DarkmodeSwitch isDarkMode={isDarkMode} toggle={toggle} />
          <hr className='w-full'/>
          <span>Username</span>
          <span>Change password</span>
          <span>More settings</span>
        </div>
      </Dropdown>
    </div>
  )
}
Header.displayName = 'Header'

export default Header
