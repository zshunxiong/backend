import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import clsx from 'clsx'
import type { FunctionComponent } from 'react'
import { useCallback, useState } from 'react'
import type { UseControllerProps } from 'react-hook-form'
import { useController, useFormContext } from 'react-hook-form'

import { InputError } from '@/components/atoms'

interface InputProps extends UseControllerProps {
  type?: 'password' | 'text'
  placeholder?: string
  readOnly?: boolean
  maxLength?: number
  autoComplete?: string
  inputClass?: string
  className?: string
}

/**
 * 自定義 react-hook-form 文字、密碼輸入欄 (必須寫在 Form 裡面)
 *
 * @param name html name 屬性
 * @param type 輸入類型 (password | text)
 * @param placeholder 預留提示文字
 * @param readOnly 是否唯獨
 * @param maxLength 輸入長度限制
 * @param autoComplete html autocomplete 屬性
 * @param inputClass input 的額外 class
 * @param className 額外 class
 */
const Input: FunctionComponent<InputProps> = ({
  name,
  type = 'text',
  placeholder,
  readOnly,
  maxLength,
  autoComplete,
  inputClass,
  className,
  defaultValue = '',
}) => {
  const { control } = useFormContext() || {}
  const { field, fieldState } = useController({
    name,
    control,
    defaultValue,
  })

  // 密碼開關
  const [showPassword, setShowPassword] = useState(false)
  const toggleShow = useCallback(() => {
    setShowPassword(!showPassword)
  }, [showPassword])

  return (
    <div className={clsx(['relative w-full', className])}>
      <input
        {...field}
        aria-label={`Input for ${name}`}
        className={clsx({
          'peer w-full rounded-md border bg-container-normal/50 px-3 py-2 shadow-sm transition-all placeholder:text-gray-400 focus:ring-2 outline-none': true,
          'border-red-400 focus:border-red-400 focus:ring-red-400/30 dark:focus:ring-red-400/50': fieldState.invalid,
          'border-gray-300 focus:border-primary focus:ring-primary/25 dark:focus:ring-primary/50': !fieldState.invalid,
          'bg-gray-200 text-text-normal/50 !cursor-not-allowed': readOnly,
          'pr-9': type === 'password',
          inputClass,
        })}
        type={!showPassword ? type : 'text'}
        autoComplete={type === 'password' ? 'current-password' : (autoComplete || 'text')}
        placeholder={placeholder}
        readOnly={readOnly}
        aria-readonly={readOnly}
        maxLength={maxLength}
      />
      {
        type === 'password'
        && (
          <FontAwesomeIcon
            icon={showPassword ? faEyeSlash : faEye}
            className="absolute right-2 top-1/2 -translate-y-1/2 cursor-pointer text-gray-400"
            onClick={toggleShow}
          />
        )
      }
      {
        fieldState.error?.message
        && <InputError> { fieldState.error?.message } </InputError>
      }
    </div>
  )
}
Input.displayName = 'Input'

export default Input
