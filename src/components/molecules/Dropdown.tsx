import type { MotionStyle } from 'framer-motion'
import { AnimatePresence, m } from 'framer-motion'
import type { FunctionComponent, ReactElement, ReactNode } from 'react'
import { cloneElement, useCallback, useMemo, useRef, useState } from 'react'
import { useIsomorphicLayoutEffect, useOnClickOutside, useToggle, useWindowSize } from 'usehooks-ts'

import { Portal } from '@/components/atoms'

interface DropdownProps {
  as: ReactElement
  direction?: 'top' | 'bottom'
  children: ReactNode
}

interface DropdowntogglePos {
  style: MotionStyle
  animate: {
    initial: number
    animate: number
    exit: number
  }
}

/**
 * 自定義下拉 dropdown 邏輯套件
 *
 * @param as 此下拉式的開關要用什麼
 * @param direction 展開的方向，目前有 'top' | 'bottom'
 * @param children ReactNode (下拉式 menu 內容)
 */
const Dropdown: FunctionComponent<DropdownProps> = ({
  as,
  direction = 'bottom',
  children,
}) => {
  const [show, toggle, setShow] = useToggle()
  const [togglePos, setTogglePos] = useState<DOMRect | null>(null)
  const { width, height } = useWindowSize()

  // 取得開關的 ref (menu 要長在他下面)
  const toggleRef = useRef<HTMLElement>(null)

  // 頁面大小變動，更新位置
  useIsomorphicLayoutEffect(() => {
    if (toggleRef.current)
      setTogglePos(toggleRef.current.getBoundingClientRect())
  }, [direction, width, height])

  // 開啟之前，再確認一次位置的正確性
  const handleToggle = useCallback(() => {
    if (toggleRef.current)
      setTogglePos(toggleRef.current.getBoundingClientRect())
    toggle()
  }, [toggle])

  // 取得 menu 的 ref (開啟後，點擊它以外的地方要觸發關閉)
  const dropdownRef = useRef<HTMLDivElement>(null)
  useOnClickOutside(dropdownRef, (event) => {
    const el = toggleRef?.current
    if (!el || el.contains(event.target as Node))
      return
    setShow(false)
  })

  // 按照 direction，預處理要帶入的位置參數
  const dropdownPos = useMemo(() => {
    if (!togglePos)
      return null

    let defaultPos: DropdowntogglePos = {
      style: {
        top: togglePos.bottom,
        right: window.innerWidth - togglePos.right,
      },
      animate: {
        initial: -togglePos.height / 2,
        animate: 0,
        exit: -togglePos.height / 2,
      },
    }

    if (direction === 'top') {
      defaultPos = {
        style: {
          bottom: window.innerHeight - togglePos.top,
          right: window.innerWidth - togglePos.right,
        },
        animate: {
          initial: togglePos.height / 2,
          animate: 0,
          exit: togglePos.height / 2,
        },
      }
    }

    return defaultPos
  }, [direction, togglePos])

  return (
    <>
      {cloneElement(as, { ref: toggleRef, onClick: handleToggle })}
      <Portal>
        <AnimatePresence>
          {show && dropdownPos
            && (
            <m.div
              ref={dropdownRef}
              style={dropdownPos.style}
              initial={{ opacity: 0, y: dropdownPos.animate.initial }}
              animate={{ opacity: 1, y: dropdownPos.animate.animate }}
              exit={{ opacity: 0, y: dropdownPos.animate.exit }}
              transition={{ duration: 0.15 }}
              className='absolute'
            >
              {children}
            </m.div>
            )
          }
        </AnimatePresence>
      </Portal>
    </>
  )
}
Dropdown.displayName = 'Dropdown'

export default Dropdown
