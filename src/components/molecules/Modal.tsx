import clsx from 'clsx'
import type { FunctionComponent, KeyboardEvent, MouseEvent, ReactNode } from 'react'
import ReactModal from 'react-modal'

export interface ModalProps {
  show?: boolean
  close?: (event?: MouseEvent<Element, globalThis.MouseEvent> | KeyboardEvent<Element>) => void
  onClose?: ReactModal.Props['onAfterClose']
  title?: string
  className?: string
  children?: ReactNode | ReactNode[]
}

/**
 * 自定義 react-modal
 *
 * @param show 是否顯示
 * @param close 關閉彈窗的方法
 * @param children ReactNode
 */
const Modal: FunctionComponent<ModalProps> = ({
  show = false,
  close,
  onClose,
  title,
  className,
  children,
}) => {
  return (
    <ReactModal
      isOpen={show}
      onRequestClose={close}
      closeTimeoutMS={300}
      overlayClassName='fixed inset-0 flex items-center justify-center bg-container-inverse/50 backdrop-blur-sm transition duration-300 z-10'
      className={clsx(['relative mx-4 w-11/12 max-w-3xl overflow-hidden rounded bg-container-normal p-4 transition duration-300 focus-visible:outline-none', className])}
      bodyOpenClassName={null}
      onAfterClose={onClose}
    >
      {
        title && (
          <div className="mb-2 w-full text-center text-primary">
            <b className="bg-container-normal px-3 text-xl font-bold">{ title }</b>
            <hr className="-mt-3.5 border-t-2 border-primary pt-3.5" />
          </div>
        )
      }
      {children}
    </ReactModal>
  )
}
Modal.displayName = 'Modal'

export default Modal
