import clsx from 'clsx'
import type { FunctionComponent, ReactNode } from 'react'

interface InputErrorProps {
  show?: boolean
  children: ReactNode
}

/**
 * 輸入欄錯誤提示 UI
 *
 * @param show 是否顯示
 * @param children ReactNode
 */
const InputError: FunctionComponent<InputErrorProps> = ({
  show,
  children,
}) => {
  return (
    <div
    className={clsx({
      'absolute bottom-[105%] right-0 z-10 mb-2 w-max scale-0 rounded-xl bg-red-500 px-2 py-0.5 transition-transform after:absolute after:right-1/4 after:block peer-focus:scale-100 after:border-8 after:border-b-0 after:border-red-500 after:border-x-transparent after:content-[""]': true,
      'scale-100': show,
    })}
  >
    <span className="text-sm text-white">
      {children}
    </span>
  </div>
  )
}
InputError.displayName = 'InputError'

export default InputError
