import { useRouter } from 'next/router'
import NProgress from 'nprogress'
import type { FunctionComponent } from 'react'
import { useEffect } from 'react'

interface LoadingBarProps {
}

/**
 * 包裝 nprogress，上方讀取進度條
 *
 */
const LoadingBar: FunctionComponent<LoadingBarProps> = () => {
  const router = useRouter()
  NProgress.configure({
    showSpinner: false,
  })

  useEffect(() => {
    router.events.on('routeChangeStart', NProgress.start)
    router.events.on('routeChangeComplete', NProgress.done)

    return () => {
      router.events.off('routeChangeStart', NProgress.start)
      router.events.off('routeChangeComplete', NProgress.done)
    }
  }, [router.events])

  return null
}
LoadingBar.displayName = 'LoadingBar'

export default LoadingBar
