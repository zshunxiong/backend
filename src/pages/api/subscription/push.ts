import { ObjectId } from 'mongodb'
import webpush from 'web-push'

import subscription from '@/lib/models/subscription'
import nc from '@/lib/nextConnect'

const router = nc()

router
  // 發送推播
  .post(async (req, res) => {
    // 檢查設定檔
    if (process.env.VAPID_PUBLIC_KEY && process.env.VAPID_PRIVATE_KEY) {
      // 設定必要資訊
      webpush.setVapidDetails(
        'mailto:example@gmail.com', // 網站的 url 或 email
        process.env.VAPID_PUBLIC_KEY,
        process.env.VAPID_PRIVATE_KEY,
      )
    }
    else { return res.json({ retCode: 1, retMsg: '推播金鑰尚未設定' }) }

    const { userid, message, ids } = req.body
    const collection = await subscription()

    let subs = []
    // 用 id 陣列推播
    if (ids) {
      const object_ids = ids.map((id: string) => new ObjectId(id))
      subs = await collection.find(
        { _id: { $in: object_ids }, skip: false },
      ).toArray()
      if (!subs.length)
        return res.json({ retCode: 1, retMsg: 'ids中查無可推播的資料' })
    }
    else {
      // 用使用者帳號推播
      subs = await collection.find(
        { userid, skip: false },
      ).toArray()
      if (!subs.length)
        return res.json({ retCode: 1, retMsg: '此帳號查無需推播的資料' })
    }

    // 有找到可推播的資料
    if (subs.length) {
      // forEach 不吃 async/await，改用 for
      for (let index = 0; index < subs.length; index++) {
        const { keys, endpoint, userid } = subs[index]
        await webpush.sendNotification(
          // 推播必要資料
          { keys, endpoint },
          // 會傳到前端 notification 的資料
          JSON.stringify({
            title: `${userid} 您好!`,
            content: message,
          }),
        )
      }
      res.json({ retCode: 0, retMsg: `已成功推送 ${subs.length} 則推播` })
    }
  })

export default router.handler()
