import { Head, Html, Main, NextScript } from 'next/document'
import { Fragment } from 'react'

import appleSplashSpecs from '@/utils/appleSplashSpecs.json'

export default function Document() {
  return (
    <Html lang="en">
      <Head>
        <meta name="description" content="Backend api server" />
        <meta name="theme-color" content="#000" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <link rel="manifest" href="/manifest.json" />
        <link rel="icon" href="/favicon.ico" />
        <link rel="icon" href="/icon/icon.svg" type="image/svg+xml" />
        <link rel="apple-touch-icon" href="/icon/apple-icon-180.png" />
        {
          appleSplashSpecs.map(({ href, media, ratio }, index) => (
            <Fragment key={index}>
              <link
                rel="apple-touch-startup-image"
                href={`/icon/apple-splash-${href.width}-${href.height}.jpg`}
                media={`(device-width: ${media.width}px) and (device-height: ${media.height}px) and (-webkit-device-pixel-ratio: ${ratio}) and (orientation: portrait)`}
              />
              <link
                rel="apple-touch-startup-image"
                href={`/icon/apple-splash-${href.height}-${href.width}.jpg`}
                media={`(device-width: ${media.width}px) and (device-height: ${media.height}px) and (-webkit-device-pixel-ratio: ${ratio}) and (orientation: landscape)`}
              />
            </Fragment>
          ))
        }
      </Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}
