import '@/styles/variables.css'
import '@/styles/globals.css'
import '@/styles/modal.css'
import '@/styles/nprogress.css'
import '@fortawesome/fontawesome-svg-core/styles.css'

import { config } from '@fortawesome/fontawesome-svg-core'
import { domAnimation, LazyMotion } from 'framer-motion'
import type { AppProps } from 'next/app'
import ReactModal from 'react-modal'

import Layout from '@/components/templates/Layout'
import { AxiosInterceptor } from '@/contexts/ApiContext'
import { DialogProvider } from '@/contexts/DialogContext'
import { LoadingProvider } from '@/contexts/LoadingContext'

// 取消 fontawesome inline css (解決弱掃問題)
config.autoAddCss = false

// https://reactcommunity.org/react-modal/accessibility/#app-element
ReactModal.setAppElement('main')

export default function App({ Component, pageProps }: AppProps) {
  return (
    /* loading 遮罩 */
    <LoadingProvider>
      {/* 彈窗 */}
      <DialogProvider>
        {/* axios 中間層和 swr 處理 SSR 資料 */}
        <AxiosInterceptor fallback={pageProps.fallback || {}}>
          {/* 動畫 */}
          <LazyMotion features={domAnimation}>
            <Layout>
              <Component {...pageProps} />
            </Layout>
          </LazyMotion>
        </AxiosInterceptor>
      </DialogProvider>
    </LoadingProvider>
  )
}
